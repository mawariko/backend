const {
    fighter
} = require('../models/fighter');
const FighterService = require('../services/fighterService');

const patterns = {
    name: /[A-Z][a-z]{1,20}/
}

function validate(regex, value, message, res) {
    if (value !== undefined && regex.test(value)) {
        return true;
    } else {
        res.err = Error(message);
        return false;
    }
}

function blockFields(data, res) {
    let valid = true;
    Object.keys(data).forEach(key => {
        if (key === 'id' || fighter[key] === undefined) {
            valid = false;
            res.err = Error(`Not supported field: ${key}`);
        }
    });
    return valid;
}

const createFighterValid = (req, res, next) => {
    let validCreate = true;
    const validatedFighter = {};

    const duplicateFighter = FighterService.search({
        name: req.body.name
    });

    if (duplicateFighter) {
        validCreate = false;
        res.err = Error('Fighter with such name already exists');
    }

    if (!(validate(patterns.name, req.body.name, "Names begin with Capitals and are at least 2 characters", res))) {
        validCreate = false;
    }

    if (!Number.isInteger(req.body.power) || req.body.power <= 0 || req.body.power >= 100) {
        validCreate = false;
        res.err = Error('Power must be between 0 and 100');
    }

    if (validCreate) {
        validCreate = blockFields(req.body, res);
    }

    req.valid = validCreate;


    if (req.valid) {
        Object.keys(fighter).forEach((key) => {
            validatedFighter[key] = req.body[key] !== undefined ? req.body[key] : fighter[key];
        })
    }

    req.validatedBody = validatedFighter;
    next();
}

const updateFighterValid = (req, res, next) => {
    let validUpdate = true;

    if (!FighterService.search({id: req.params.id})) {
        validUpdate = false;
        res.err = Error('Can not find fighter with such id');
    }

    if (validUpdate && req.body.name) {
        if (!(validate(patterns.name, req.body.name, "Names begin with Capitals and are at least 2 characters", res))) {
            validUpdate = false;
        }
    }
    if (validUpdate && req.body.power) {
        if (!((req.body.power > 0 && req.body.power <= 100))) {
            validUpdate = false;
            res.err = Error('Power must be between 0 and 100');
        }
    }

    // Made them just in case, since I haven't found the update form and don't know what can be updated
    // if (validUpdate && req.body.defense) {
    //     if (!(req.body.defense > 1 && req.body.defense <= 10)) {
    //         validUpdate = false;
    //         res.err = Error('Defense must be between 0 and 10');
    //     }
    // }
    // if (validUpdate && req.body.health) {
    //     if (!(req.body.health > 1 && req.body.health <= 100)) {
    //         validUpdate = false;
    //         res.err = Error('Health must be between 0 and 100');
    //     }
    // }

    if (validUpdate) {
        validUpdate = blockFields(req.body, res);
    }

    req.valid = validUpdate;
    req.validatedBody = req.body;
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;