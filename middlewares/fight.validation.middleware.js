const {
    fight
} = require('../models/fight');
const FighterService = require('../services/fighterService');

const createFightValid = (req, res, next) => {
    let valid = true;
    const newFight = {};

    if(!(FighterService.search({id: req.body.fighter1}) && FighterService.search({id: req.body.fighter2}))) {
        err.status = 400;
        res.err = new Error("Cannot find fighter with such id");
        valid = false;
    }

    req.valid = valid;

    if (req.valid) {
        Object.keys(fight).forEach((key) => {
            if (key !== "id" && key !== "log") {
                if (req.body[key] !== undefined) {
                    newFight[key] = req.body[key];
                } else {
                    newFight[key] = fight[key];
                }
            }
        })
    }

    req.validatedBody = newFight;
    next();
}

exports.createFightValid = createFightValid;