const {
    user
} = require('../models/user');
const UserService = require('../services/userService');


const patterns = {
    phone: /(\+380)(\d{9})/,
    email: /^([a-zA-Z0-9_\-\.]+)(@gmail.com)$/,
    notempty: /^\S+$/,
    password: /^.{3,}$/
};

function validate(regex, value, message, res) {
    if (value !== undefined && regex.test(value)) {
        return true;
    } else {
        res.err = Error(message);
        return false;
    }
}

function blockFields(data, res) {
    let valid = true;
    Object.keys(data).forEach(key => {
        if (key === 'id' || user[key] === undefined) {
            valid = false;
            res.err = Error(`Not supported field: ${key}`);
        }
    });
    return valid;
}


const createUserValid = (req, res, next) => {
    let validCreate = true;

    const duplicateEmailUser = UserService.search({
        email: req.body.email
    });

    if (duplicateEmailUser) {
        validCreate = false;
        res.err = Error('User with such email already exists');
    }

    validCreate = validCreate &&
        validate(patterns.phone, req.body.phoneNumber, "Number should be +380*********", res) &&
        validate(patterns.email, req.body.email, "Only gmail emails are accepted", res) &&
        validate(patterns.password, req.body.password, "Password must be at least 3 characters long and have at least one special character", res) &&
        validate(patterns.notempty, req.body.firstName, "Fill in your first name", res) &&
        validate(patterns.notempty, req.body.lastName, "Fill in your second name", res) &&
        blockFields(req.body, res);

    req.valid = validCreate;
    req.validatedBody = req.body;
    next();
}


const updateUserValid = (req, res, next) => {
    let validUpdate = true;

    if(!UserService.search({id: req.params.id})) {
        validUpdate = false;
        res.err = Error('Can not find user with such id');
    }

    if (validUpdate && req.body.firstName) {
        validUpdate = validate(patterns.notempty, req.body.firstName, "Fill in your first name", res);
    }
    if (validUpdate && req.body.lastName) {
        validUpdate = validate(patterns.notempty, req.body.lastName, "Fill in your second name", res);
    }
    if (validUpdate && req.body.email) {
        validUpdate = validate(patterns.email, req.body.email, "Only gmail emails are accepted", res);
    }
    if (validUpdate && req.body.phoneNumber) {
        validUpdate = validate(patterns.phone, req.body.phoneNumber, "Number should be +380*********", res);
    }
    if (validUpdate && req.body.password) {
        validUpdate = validate(patterns.password, req.body.password, "Password must be at least 3 characters long and have at least one special character");
    } 
    if (validUpdate) {
        validUpdate = blockFields(req.body, res);
    }
   
    req.valid = validUpdate;
    req.validatedBody = req.body;
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;