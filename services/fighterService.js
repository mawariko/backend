const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll() {
        return FighterRepository.getAll();   
    }

    create(fighter) {
        const newFighter = FighterRepository.create(fighter);
        if(!newFighter) {
            throw Error("Unable to create fighter");
        }
        return newFighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const updatedFighter = FighterRepository.update(id, dataToUpdate);
        if(!updatedFighter) {
            throw Error("Unable to update this fighter");
        }
        return updatedFighter;
    }

    delete(id) {
        const fighterToDelete = FighterRepository.delete(id);
        if(!fighterToDelete) {
            throw Error("Cannot delete this fighter");
        }
        return fighterToDelete;
    }
}

module.exports = new FighterService();