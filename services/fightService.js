const { FightRepository } = require('../repositories/fightRepository');

class FightService {
    // OPTIONAL TODO: Implement methods to work with fights
    create(fight) {
        const newFight = FightRepository.create(fight);
        if(!newFight) {
            throw Error("Unable to start a fight");
        }
        return newFight;
    }

}

module.exports = new FightService();