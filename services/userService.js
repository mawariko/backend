const {
    UserRepository
} = require('../repositories/userRepository');
const {
    user: userModel
} = require('../models/user');

class UserService {
    //  
    // TODO: Implement methods to work with user
    getAll() {
        return UserRepository.getAll();
    }

    create(user) {
        const newUser = UserRepository.create(user);
        if (!newUser) {
            throw Error("Unnable to create user");
        }
        return newUser;
    }

    delete(id) {
        const userToDelete = UserRepository.delete(id);
        if (!userToDelete) {
            throw Error("Cannot delete this user");
        }
        return userToDelete;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const updatedUser = UserRepository.update(id, dataToUpdate);
        if (!updatedUser) {
            throw Error("Unnable to update this user");
        }
        return updatedUser;
    }
}

module.exports = new UserService();