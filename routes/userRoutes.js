const {
    Router
} = require('express');
const UserService = require('../services/userService');
const {
    createUserValid,
    updateUserValid
} = require('../middlewares/user.validation.middleware');
const {
    responseMiddleware
} = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
   let allUsers = UserService.getAll();
   res.data = allUsers;
   next();
}, responseMiddleware)


router.get('/:id', (req, res, next) => {
    const chosenUser = UserService.search({id: req.params.id});

    if(chosenUser) {
        res.data = chosenUser;
    } else {
        const e = new Error("Cannot find user with such id");
        e.status = 404
        res.err = e;
    }
  
    next();
}, responseMiddleware)


router.post('/', createUserValid, (req, res, next) => {
    if(req.valid) {
       const newUser = UserService.create(req.validatedBody)
       res.data = newUser;
    }
    next();
}, responseMiddleware)


router.put('/:id', updateUserValid, (req, res, next) => {
    if(req.valid) {
        const updatedUser = UserService.update(req.params.id, req.validatedBody);
        res.data = updatedUser;
    }
    next();
}, responseMiddleware)


router.delete('/:id', (req, res, next) => {
    let userToDelete = UserService.delete(req.params.id)
    res.data = userToDelete;
    next();
 }, responseMiddleware)
 

module.exports = router;
// USER:
// GET /api/users
// GET /api/users/:id
// POST /api/users
// PUT /api/users/:id
// DELETE /api/users/:id