const {
    Router
} = require('express');
const FighterService = require('../services/fighterService');
const {
    responseMiddleware
} = require('../middlewares/response.middleware');
const {
    createFighterValid,
    updateFighterValid
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

// GET ALL
router.get('/', (req, res, next) => {
    let allFighters = FighterService.getAll();
    res.data = allFighters;
    next()
}, responseMiddleware)


// GET ONE
router.get('/:id', (req, res, next) => {
    const chosenFighter = FighterService.search({
        id: req.params.id
    });

    if(chosenFighter) {
        res.data = chosenFighter;
    } else {
        const e = new Error("Cannot find fighter with such id");
        e.status = 404;
        res.err = e;
    }
   
    next()
}, responseMiddleware)


// CREATE
router.post('/', createFighterValid, (req, res, next) => {
    if (req.valid) {
        const newFighter = FighterService.create(req.validatedBody)
        res.data = newFighter;
    }

    next()
}, responseMiddleware)


// UPDATE
router.put('/:id', updateFighterValid, (req, res, next) => {
    if(req.valid) {
        const updatedFighter = FighterService.update(req.params.id, req.validatedBody);
        res.data = updatedFighter;
    }

    next();
}, responseMiddleware)



// DELETE
router.delete('/:id', (req, res, next) => {
    let fighterToDelete = FighterService.delete(req.params.id)
    res.data = fighterToDelete;
    
    next()
 }, responseMiddleware)
 

module.exports = router;