const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFightValid } = require('../middlewares/fight.validation.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.post('/', createFightValid, (req, res, next) => {
    if (req.valid) {
        const newFight = FightService.create(req.validatedBody)
        res.data = newFight;
    }

    next();
}, responseMiddleware)

module.exports = router;